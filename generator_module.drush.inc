<?php

/**
 * @file
 * File Drush command for module generator_module.
 */

use GeneratorModule\FileCollection;

/**
 * Returns a list of commands for generator_module.
 *
 * @return array
 *   List of command line
 */
function generator_module_drush_command() {
  $commands = array();

  $commands['generate-module'] = array(
    'callback'    => 'generator_module_drush_generate_module',
    'description' => t('Generate a module empty.'),
    'bootstrap'   => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments'   => array(),
    'options'     => array(
      '--path'        => 'The directory where to create the module',
      '--name'        => 'The module name',
      '--description' => 'The optional module description'
    ),
    'examples'    => array(),
  );

  $commands['generate-theme'] = array(
    'callback'    => 'generator_module_drush_generate_theme',
    'description' => t('Generate a theme empty.'),
    'bootstrap'   => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'arguments'   => array(),
    'options'     => array(
      '--path'        => 'The directory where to create the theme',
      '--name'        => 'The theme name',
      '--description' => 'The optional theme description'
    ),
    'examples'    => array(),
  );

  return $commands;
}

/**
 * Callback command of drush "generate-module".
 */
function generator_module_drush_generate_module() {
  $yes = drush_get_context('DRUSH_AFFIRMATIVE');
  drush_set_context('DRUSH_AFFIRMATIVE', FALSE);

  $path = drush_get_option('path');

  if (!$path) {
    $path = DRUPAL_ROOT . '/sites/all/modules';
    $path = $yes? $path: drush_prompt(dt('Path to module'), $path);
  }

  $name = drush_get_option('name');
  while (!_generator_module_drush_name_validate($path, $name)) {
    $name = drush_prompt(dt('Name'));
  }

  $variables = array();
  $variables['module_name'] = $name;

  $variables['module_description'] = drush_get_option('description');
  if (!$variables['module_description']) {
    $variables['module_description'] = drush_prompt(dt('Description'), $name);
  }

  if (!$yes && !drush_confirm(dt('Do you confirm generation ?'))) {
    return drush_user_abort();
  }

  $header_php_file = '<?php' . "\n\n";

  $renders = new FileCollection('module', $path . '/' . $name, $variables);
  $renders->addRender('module', $name . '.module', $variables, $header_php_file);
  $renders->addRender('install', $name . '.install', $variables, $header_php_file);
  $renders->addRender('info', $name . '.info', $variables);
  $renders->addRender('drush', $name . '.drush.inc', $variables, $header_php_file);
  $renders->addRender('function', 'inc/function.inc', $variables, $header_php_file);
  $renders->addRender('preprocess', 'inc/preprocess.inc', $variables, $header_php_file);
  $renders->addRender('readme', 'public/README.txt', array('definition' => 'assets'));
  $renders->addRender('readme', 'templates/README.txt', array('definition' => 'assets'));

  _generator_module_drush_generate($renders);

  drush_print('Done.');
}

/**
 * Callback command of drush "generate-theme".
 */
function generator_module_drush_generate_theme() {
  $yes = drush_get_context('DRUSH_AFFIRMATIVE');
  drush_set_context('DRUSH_AFFIRMATIVE', FALSE);

  $path = drush_get_option('path');

  if (!$path) {
    $path = DRUPAL_ROOT . '/sites/all/themes';
    $path = $yes? $path: drush_prompt(dt('Path to theme'), $path);
  }

  $name = drush_get_option('name');
  while (!_generator_module_drush_name_validate($path, $name)) {
    $name = drush_prompt(dt('Name'));
  }

  $variables = array();
  $variables['theme_name'] = $name;

  $variables['theme_description'] = drush_get_option('description');
  if (!$variables['theme_description']) {
    $variables['theme_description'] = drush_prompt(dt('Description'), $name);
  }

  if (!$yes && !drush_confirm(dt('Do you confirm generation ?'))) {
    return drush_user_abort();
  }

  $header_php_file = '<?php' . "\n\n";

  $renders = new FileCollection('theme', $path . '/' . $name, $variables);
  $renders->addRender('template', 'template.php', $variables, $header_php_file);
  $renders->addRender('info', $name . '.info', $variables);
  $renders->addRender('preprocess_node', 'inc/preprocess_node.inc', $variables, $header_php_file);
  $renders->addRender('function', 'inc/function.inc', $variables, $header_php_file);

  foreach (array('block', 'comment', 'html', 'maintenance-page', 'page', 'region') as $static) {
    $renders->addStatic($static . '.tpl.php', sprintf('templates/%s.tpl.php', $static));
  }

  _generator_module_drush_generate($renders);

  drush_print('Done.');
}

/**
 * Supports generate file collection with the drush contexts.
 *
 * @param FileCollection $renders
 *   An instance of file collection.
 */
function _generator_module_drush_generate(FileCollection $renders) {
  // call alter generator
  drupal_alter("generator_module_render", $renders);

  if (drush_get_context('DRUSH_VERBOSE')) {
    foreach ($renders->getFiles() as $file) {
      drush_print(sprintf('Create: %s/%s', $renders->getPath(), $file->getTarget()));

      if (!drush_get_context('DRUSH_SIMULATE')) {
        $file->generate($renders->getSkeleton(), $renders->getPath());
      }
    }
  } elseif (!drush_get_context('DRUSH_SIMULATE')) {
    $renders->generate();
  }
}

/**
 * Validate the name of a feature, module or theme for example.
 *
 * @param string $path
 *   Path to feature
 * @param string $name
 *   Name of feature
 *
 * @return boolean
 *   True on success
 */
function _generator_module_drush_name_validate($path, $name) {
  if (!$name) {
    return FALSE;
  }

  if (!preg_match('/^[a-z_\x7f-\xff][a-z0-9_\x7f-\xff]*$/', $name)) {
    drush_print('Theme name contains invalid characters.');

    return FALSE;
  }
  elseif (is_dir($path . DIRECTORY_SEPARATOR . $name)) {
    drush_print(sprintf('The directory %s/%s already exists.', $path, $name));

    return FALSE;
  }

  return TRUE;
}
