Generator Module
================

**Requires** at least *PHP 5.3.3* with Drush.
The generator module is a sandbox generator of components for your drupal web-site, it's several command line shell from drush, for generate drupal module or other elements.

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/65b9b117-4292-4fe9-b582-395dd6397849/mini.png)](https://insight.sensiolabs.com/projects/65b9b117-4292-4fe9-b582-395dd6397849)

Branch  | PHP | Compatible Drupal versions | Code Status
------  | --- | -------------------------- | -----------
master  | 5.3.3+ | D8 | [![Build Status](https://travis-ci.org/pokap/generator_module.png?branch=master)](https://travis-ci.org/pokap/generator_module)
7.x-1.x | 5.3.3+ | D7 | [![Build Status](https://travis-ci.org/pokap/generator_module.png?branch=7.x-1.x)](https://travis-ci.org/pokap/generator_module)

Full test suite powered by [PHPUnit](https://github.com/sebastianbergmann/phpunit).


Installation
-----------

You must install module in your website.
The generator module use hook theme for override render file, if you use drupal sandbox for example.

You can install it with drush :

    $ drush en generator_module


USAGE
-----------

You can be run in your shell by drush.

    $ drush generate-module

It created module like directory:

    ├── inc
    │   ├── function.inc
    │   ├── preprocess.inc
    ├── public
    │   ├── README.txt
    ├── templates
    │   ├── README.txt
    ├── module_name.info
    ├── module_name.install
    ├── module_name.module

