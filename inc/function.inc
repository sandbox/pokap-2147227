<?php

/**
 * @file
 * File content all custom functions for module generator_module.
 */

/**
 * Generate a file to target from theme render.
 *
 * @param string $skeleton
 *   Name of sketelon type.
 * @param string $template
 *   Name of template of skeleton.
 * @param string $target
 *   Directory target for the creation of file.
 * @param array  $variables
 *   (Optional) Parameters theme for sketelon and template.
 * @param string $prefix
 *   (Optional) Header file prefix render theme.
 *
 * @return integer
 *   Number of bytes that were written to the file, or false on failure
 */
function generator_module_render_file($skeleton, $template, $target, array $variables, $prefix = '') {
  if (!is_dir(dirname($target))) {
    mkdir(dirname($target), 0777, TRUE);
  }

  $result = theme(sprintf('generator_module__%s_%s', $skeleton, $template), $variables);

  return file_put_contents($target, $prefix . $result);
}

/**
 * Copy a file from template to target.
 *
 * @param string $skeleton
 *   Name of sketelon type.
 * @param string $static
 *   Name of template of skeleton.
 * @param string $target
 *   Directory target for the creation of file.
 *
 * @return boolean
 *   True on success or false on failure.
 */
function generator_module_copy_static($skeleton, $static, $target) {
  if (!is_dir(dirname($target))) {
    mkdir(dirname($target), 0777, TRUE);
  }

  return copy(sprintf(__DIR__ . '/../skeleton/%s/static/%s', $skeleton, $static), $target);
}
