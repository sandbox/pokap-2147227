<?php

/**
 * @file
 * Generator module classes.
 *
 * Class required for generate template from theme API.
 * It used in file collection class.
 */

namespace GeneratorModule;

/**
 * File render class.
 *
 * Generate template from theme API.
 */
class FileRender implements FileInterface {

  /** @var string */
  protected $template;

  /** @var string */
  protected $target;

  /** @var array */
  protected $variables;

  /** @var string */
  protected $prefix;

  /**
   * Constructor.
   *
   * @param string $template
   *   Name of template of skeleton.
   * @param string $target
   *   Complete name file with a sub-path.
   * @param array  $variables
   *   (Optional) Parameters theme for sketelon and template.
   * @param string $prefix
   *   (Optional) Header file prefix render theme.
   */
  public function __construct($template, $target, array $variables = array(), $prefix = '') {
    $this->template  = $template;
    $this->target    = $target;
    $this->variables = $variables;
    $this->prefix    = $prefix;
  }

  /**
   * {@inheritdoc}
   */
  public function generate($skeleton, $path) {
    $path .= DIRECTORY_SEPARATOR . $this->target;

    return generator_module_render_file($skeleton, $this->template, $path, $this->variables, $this->prefix) > 0;
  }

  /**
   * Sets the prefix.
   *
   * @param string $prefix
   *   Header file prefix render theme.
   */
  public function setPrefix($prefix) {
    $this->prefix = $prefix;
  }

  /**
   * Returns the prefix.
   *
   * @return null|string
   *   Header file prefix render theme.
   */
  public function getPrefix() {
    return $this->prefix;
  }

  /**
   * Sets the target.
   *
   * @param string $target
   *   Complete name file with a sub-path.
   */
  public function setTarget($target) {
    $this->target = $target;
  }

  /**
   * {@inheritdoc}
   */
  public function getTarget() {
    return $this->target;
  }

  /**
   * Sets the template name.
   *
   * @param string $template
   *   Name of template of skeleton.
   */
  public function setTemplate($template) {
    $this->template = $template;
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplate() {
    return $this->template;
  }

  /**
   * Sets the variables.
   *
   * @param array $variables
   *   Parameters theme for sketelon and template.
   */
  public function setVariables(array $variables) {
    $this->variables = $variables;
  }

  /**
   * Sets a variable.
   *
   * @param string $name
   *   Variable name.
   * @param mixed $value
   *   Variable value.
   */
  public function setVariable($name, $value) {
    $this->variables[$name] = $value;
  }

  /**
   * Returns if a variable exists given by name.
   *
   * @param string $name
   *   Variable name.
   *
   * @return boolean
   *   True on variable exists.
   */
  public function hasVariable($name) {
    return array_key_exists($name, $this->variables);
  }

  /**
   * Remove a variable given by name.
   *
   * @param string $name
   *   Variable name.
   *
   * @throws \InvalidArgumentException
   *   When variable does not exists.
   */
  public function removeVariable($name) {
    if (!$this->hasVariable($name)) {
      throw new \InvalidArgumentException(sprintf('Variable %s not exists for template %s.', $name, $this->template));
    }

    unset($this->variables[$name]);
  }

  /**
   * Returns the variables.
   *
   * @return array
   *   Parameters theme for sketelon and template.
   */
  public function getVariables() {
    return $this->variables;
  }
}
