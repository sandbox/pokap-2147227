<?php

/**
 * @file
 * Generator module classes.
 *
 * Interface for all type file class.
 */

namespace GeneratorModule;

/**
 * File interface.
 */
interface FileInterface {

  /**
   * Generate the file type given by skeleton.
   *
   * @param string $skeleton
   *   Name of sketelon type.
   * @param string $path
   *   Path to target file.
   *
   * @return boolean
   *   True on success.
   */
  public function generate($skeleton, $path);

  /**
   * Returns the target name.
   *
   * @return string
   *   Complete name file with a sub-path.
   */
  public function getTarget();

  /**
   * Returns the template name.
   *
   * @return string
   *   Name of template of skeleton.
   */
  public function getTemplate();
}
