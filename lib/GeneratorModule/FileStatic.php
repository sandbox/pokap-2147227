<?php

/**
 * @file
 * Generator module classes.
 *
 * Class required for generate a copy from template skeleton.
 * It used in file collection class.
 */

namespace GeneratorModule;

/**
 * Static file class.
 *
 * The static file generate a copy from original template skeleton.
 */
class FileStatic implements FileInterface {

  /** @var string */
  protected $template;

  /** @var string */
  protected $target;

  /**
   * Constructor.
   *
   * @param string $template
   *   Name of template of skeleton.
   * @param string $target
   *   Complete name file with a sub-path.
   */
  public function __construct($template, $target) {
    $this->template = $template;
    $this->target   = $target;
  }

  /**
   * {@inheritdoc}
   */
  public function generate($skeleton, $path) {
    return generator_module_copy_static($skeleton, $this->template, $path . DIRECTORY_SEPARATOR . $this->target);
  }

  /**
   * Sets the target.
   *
   * @param string $target
   *   Complete name file with a sub-path.
   */
  public function setTarget($target) {
    $this->target = $target;
  }

  /**
   * {@inheritdoc}
   */
  public function getTarget() {
    return $this->target;
  }

  /**
   * Sets the template name.
   *
   * @param string $template
   *   Name of template of skeleton.
   */
  public function setTemplate($template) {
    $this->template = $template;
  }

  /**
   * {@inheritdoc}
   */
  public function getTemplate() {
    return $this->template;
  }
}
