<?php

/**
 * @file
 * Generator module classes.
 *
 * Class required for to create a collection of file render with a base skeleton and path.
 */

namespace GeneratorModule;

/**
 * Collection file class.
 *
 * This class is a collection of class implement the FileInterface.
 * It generates all the render files or static in the collection.
 */
class FileCollection {

  /** @var FileInterface[] */
  protected $files = array();

  /** @var string */
  protected $skeleton;

  /** @var string */
  protected $path;

  /** @var array */
  protected $parameters;

  /**
   * Constructor.
   *
   * @param string $skeleton
   *   Name of sketelon type.
   * @param string $path
   *   Path to target file.
   * @param array $parameters
   *   (Optional) Basic parameters used in render file.
   */
  public function __construct($skeleton, $path, array $parameters = array()) {
    $this->skeleton   = $skeleton;
    $this->path       = $path;
    $this->parameters = $parameters;
  }

  /**
   * Sets the skeleton name.
   *
   * @param string $skeleton
   *   Name of sketelon type.
   */
  public function setSkeleton($skeleton) {
    $this->skeleton = $skeleton;
  }

  /**
   * Returns the skeleton name.
   *
   * @return string
   *   Path to target file.
   */
  public function getSkeleton() {
    return $this->skeleton;
  }

  /**
   * Returns the path.
   *
   * @return string
   *   Name of sketelon type.
   */
  public function getPath() {
    return $this->path;
  }

  /**
   * Sets the path.
   *
   * @param string $path
   *   Path to target file.
   */
  public function setPath($path) {
    $this->path = $path;
  }

  /**
   * Returns the basic parameters used in render file.
   *
   * @return array
   *   List association of parameter.
   */
  public function getParameters() {
    return $this->parameters;
  }

  /**
   * Adding file type on list of files which will generate.
   *
   * @param FileInterface $file
   *   An instance of render or static file.
   *
   * @throws \InvalidArgumentException
   *   When target file is already used.
   */
  public function add(FileInterface $file) {
    if ($this->hasTarget($file->getTarget())) {
      throw new \InvalidArgumentException(sprintf('The template %s aleardy use target %s of you template %s',
        $this->files[$file->getTarget()]->getTemplate(),
        $file->getTarget(),
        $file->getTemplate()
      ));
    }

    $this->files[$file->getTarget()] = $file;
  }

  /**
   * Returns a file render or static given by target.
   *
   * @param string $target
   *   Complete name file with a sub-path.
   *
   * @return FileInterface
   *   An instance of render or static file.
   *
   * @throws \InvalidArgumentException
   *   When target file does not exists.
   */
  public function get($target) {
    if (!$this->hasTarget($target)) {
      throw new \InvalidArgumentException(sprintf('The target %s is not registered.', $target));
    }

    return $this->files[$target];
  }

  /**
   * Returns a file render or static given by target.
   *
   * @param string $target
   *   Complete name file with a sub-path.
   *
   * @return FileInterface
   *   An instance of render or static file.
   *
   * @throws \InvalidArgumentException
   *   When target file does not exists.
   */
  public function remove($target) {
    if (!$this->hasTarget($target)) {
      throw new \InvalidArgumentException(sprintf('The target %s is not registered.', $target));
    }

    unset($this->files[$target]);
  }

  /**
   * Returns if a file target is already register.
   *
   * @param string $target
   *   Complete name file with a sub-path.
   *
   * @return boolean
   *   True if target file is registered.
   */
  public function hasTarget($target) {
    return array_key_exists($target, $this->files);
  }

  /**
   * Adding file type render.
   *
   * @param string $template
   *   Name of template of skeleton.
   * @param string $target
   *   Complete name file with a sub-path.
   * @param array  $variables
   *   (Optional) Parameters theme for sketelon and template.
   * @param string $prefix
   *   (Optional) Header file prefix render theme.
   */
  public function addRender($template, $target, array $variables = array(), $prefix = '') {
    $this->add(new FileRender($template, $target, $variables, $prefix));
  }

  /**
   * Adding file type static.
   *
   * @param string $static
   *   Name of template of skeleton.
   * @param string $target
   *   Complete name file with a sub-path.
   */
  public function addStatic($static, $target) {
    $this->add(new FileStatic($static, $target));
  }

  /**
   * Returns the list of files.
   *
   * @return FileInterface[]
   *   List of instances of render or static file.
   */
  public function getFiles() {
    return $this->files;
  }

  /**
   * Generate all files registers.
   */
  public function generate() {
    foreach ($this->files as $file) {
      $file->generate($this->skeleton, $this->path);
    }
  }
}
