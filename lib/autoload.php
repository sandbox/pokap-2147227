<?php

/**
 * @file
 * Autoload classes.
 */

// If your Drupal use composer, you don't need a specific autoload.
if (class_exists('Composer\Autoload\ClassLoader')) {
  return;
}

/**
 * @param string $class
 *   Class name complete with namespace.
 */
function generator_module_autoload($class) {
  // When you have new classes, push it into classmap
  $classmap = array(
    'GeneratorModule\\FileCollection' => 'GeneratorModule/FileCollection.php',
    'GeneratorModule\\FileInterface'  => 'GeneratorModule/FileInterface.php',
    'GeneratorModule\\FileRender'     => 'GeneratorModule/FileRender.php',
    'GeneratorModule\\FileStatic'     => 'GeneratorModule/FileStatic.php',
  );

  if (isset($classmap[$class])) {
    require __DIR__ . '/' . $classmap[$class];
  }
}

spl_autoload_register('generator_module_autoload');
