<?php

/**
 * @file
 * API documentation for Generator module.
 */

use GeneratorModule\FileCollection;

/**
 * Modify generation made ​​by command line or other.
 *
 * @param FileCollection $renders
 *   The file collection used during the process of generation.
 */
function hook_generator_module_render_alter(FileCollection $renders) {

  switch ($renders->getSkeleton()) {
    case 'module':
      // You can remove a file before his generation
      if ($renders->hasTarget('inc/preprocess.inc')) {
        $renders->remove('inc/preprocess.inc');
      }

      // And you can adding your file
      if (!$renders->hasTarget('admin/config.inc')) {
        $renders->addRender('admin_config', 'admin/config.inc', $renders->getParameters(), '<?php' . "\n\n");
      }
      break;

    case 'theme':
      // You can manipulate render like the module skeleton.
      break;
  }
}
