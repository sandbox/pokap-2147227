<?php

define('DRUPAL_ROOT', __DIR__ . '/..');

chdir(DRUPAL_ROOT);

require_once __DIR__ . '/../generator_module.module';
require_once __DIR__ . '/../generator_module.drush.inc';
require_once __DIR__ . '/../generator_module.api.php';

// ----------------
// mocks

/**
 * @param string $hook
 * @param array  $variables
 *
 * @return string
 */
function theme($hook, array $variables = array()) {
  $themes = generator_module_theme();

  ob_start();
  extract($variables);
  include __DIR__ . '/../' . $themes[$hook]['template'] . '.tpl.php';

  $content = ob_get_contents();
  ob_clean();

  return $content;
}

/**
 * @return array
 */
function drupal_alter() {}
