<?php

define('BASE_PATH', sys_get_temp_dir() . DIRECTORY_SEPARATOR . 'generator_module/' . sha1(uniqid()));
define('MODULE_PATH', BASE_PATH . DIRECTORY_SEPARATOR . 'modules');
define('THEME_PATH', BASE_PATH . DIRECTORY_SEPARATOR . 'themes');

/**
 * GeneratorModuleCommandTest
 */
class GeneratorModuleCommandTest extends PHPUnit_Framework_TestCase {

  /**
   * Test generator module command.
   */
  public function testGeneratorModule() {
    generator_module_drush_generate_module();

    $this->assertTrue(is_dir(MODULE_PATH . '/test'));
    $this->assertTrue(is_dir(MODULE_PATH . '/test/inc'));
    $this->assertTrue(is_dir(MODULE_PATH . '/test/public'));
    $this->assertTrue(is_dir(MODULE_PATH . '/test/templates'));
    $this->assertTrue(file_exists(MODULE_PATH . '/test/test.module'));
    $this->assertTrue(file_exists(MODULE_PATH . '/test/test.install'));
    $this->assertTrue(file_exists(MODULE_PATH . '/test/test.info'));

    exec(sprintf('rm -r %s', MODULE_PATH));
  }

  public function testGeneratorTheme() {
    generator_module_drush_generate_theme();

    $this->assertTrue(is_dir(THEME_PATH . '/test'));
    $this->assertTrue(is_dir(THEME_PATH . '/test/inc'));
    $this->assertTrue(is_dir(THEME_PATH . '/test/templates'));
    $this->assertTrue(file_exists(THEME_PATH . '/test/template.php'));
    $this->assertTrue(file_exists(THEME_PATH . '/test/test.info'));
    $this->assertTrue(file_exists(THEME_PATH . '/test/inc/function.inc'));
    $this->assertTrue(file_exists(THEME_PATH . '/test/inc/preprocess_node.inc'));
    $this->assertTrue(file_exists(THEME_PATH . '/test/templates/block.tpl.php'));
    $this->assertTrue(file_exists(THEME_PATH . '/test/templates/comment.tpl.php'));
    $this->assertTrue(file_exists(THEME_PATH . '/test/templates/html.tpl.php'));
    $this->assertTrue(file_exists(THEME_PATH . '/test/templates/maintenance-page.tpl.php'));
    $this->assertTrue(file_exists(THEME_PATH . '/test/templates/page.tpl.php'));
    $this->assertTrue(file_exists(THEME_PATH . '/test/templates/region.tpl.php'));

    exec(sprintf('rm -r %s', THEME_PATH));
  }
}

function drush_confirm() {
  return true;
}

function drush_print() {}

function dt($text) {
  return $text;
}

function drush_prompt($name) {
  switch ($name) {
    case 'Path to module':
      return MODULE_PATH;

    case 'Path to theme':
      return THEME_PATH;

    case 'Name':
    case 'Description':
      return 'test';
  }

  throw new \RuntimeException(sprintf('Unknown display title "%s"', $name));
}

function drush_get_context() {
    return false;
}

function drush_get_option() {
    return false;
}

function drush_set_context() {}
