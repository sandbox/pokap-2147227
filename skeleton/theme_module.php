<?php

/**
 * @file
 * List themes for skeleton module.
 */

$themes = array(
  'generator_module__module_module' => array(
    'template' => 'skeleton/module/module',
  ),
  'generator_module__module_install' => array(
    'template' => 'skeleton/module/install',
  ),
  'generator_module__module_info' => array(
    'template' => 'skeleton/module/info',
  ),
  'generator_module__module_function' => array(
    'template' => 'skeleton/module/function.inc',
  ),
  'generator_module__module_preprocess' => array(
    'template' => 'skeleton/module/preprocess.inc',
  ),
  'generator_module__module_drush' => array(
    'template' => 'skeleton/module/drush.inc',
  ),
  'generator_module__module_readme' => array(
    'template' => 'skeleton/module/README.txt',
  )
);

return $themes;
