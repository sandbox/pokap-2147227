<?php

/**
 * @file
 * List themes for skeleton theme.
 */

$themes = array(
  'generator_module__theme_template' => array(
    'template' => 'skeleton/theme/template',
  ),
  'generator_module__theme_info' => array(
    'template' => 'skeleton/theme/info',
  ),
  'generator_module__theme_function' => array(
    'template' => 'skeleton/theme/function.inc',
  ),
  'generator_module__theme_preprocess_node' => array(
    'template' => 'skeleton/theme/preprocess_node.inc',
  )
);

return $themes;
