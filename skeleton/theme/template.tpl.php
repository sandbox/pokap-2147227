/**
 * @file
 * File content principal hook for theme <?php echo $theme_name ?>.
 */

require_once 'inc/function.inc';
require_once 'inc/preprocess_node.inc';

/**
 * Override or insert variables into the maintenance page template.
 */
function <?php echo $theme_name ?>_preprocess_maintenance_page(&$vars) {
  // While markup for normal pages is split into page.tpl.php and html.tpl.php,
  // the markup for the maintenance page is all in the single
  // maintenance-page.tpl.php template. So, to have what's done in
  // <?php echo $theme_name ?>_preprocess_html() also happen on the maintenance page, it has to be
  // called here.
  <?php echo $theme_name ?>_preprocess_html($vars);
}

/**
* Override or insert variables into the html template.
*/
function <?php echo $theme_name ?>_process_html(array &$vars) {

}

/**
* Override or insert variables into the html template.
*/
function <?php echo $theme_name ?>_preprocess_html(array &$vars) {

}

/**
* Override or insert variables into the page template.
*/
function <?php echo $theme_name ?>_preprocess_page(array &$vars) {

}

/**
* Override or insert variables into the node template.
*/
function <?php echo $theme_name ?>_preprocess_node(array &$vars) {
  $function = sprintf('<?php echo $theme_name ?>_preprocess_node_%s', $vars['node']->type);

  if (function_exists($function)) {
    $function($vars);
  }
}
