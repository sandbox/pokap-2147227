/**
 * @file
 * File content principal hook for module <?php echo $module_name ?>.
 */

require_once 'inc/preprocess.inc';
require_once 'inc/function.inc';

/**
 * Implementation of hook_theme().
 *
 * @return array
 */
function <?php echo $module_name ?>_theme() {
  return array();
}
